/*
Go to https://www.facebook.com/{username}/photos_of
  OR
Go to https://www.facebook.com/{username}/photos_all
Currently can't switch using the "Photos of You" and "Your Photos" text buttons
so if you do want to switch enter one of the above URLs manually

To load the tags for all the images you'll have to scroll down and manually
load them all
*/

// May God forgive me for these global variables
// id: [tag list]
var gPhotoTags;
// tag: count
var gTagCounts;
// Currently selected tags
var gSelectedTags = [];
// Number of currently loaded photos
var gPhotoCount = 0;

$(document).ready(function() {
  var collectionWrapper = $('div[id^="collection_wrapper"]');
  $(collectionWrapper).css("margin-top", "10px");
  initSearch();
  refreshTags();
  // Check every 500ms to see if new images have been loaded
  setInterval(function(){
    checkImageCount();
  }, 500);
});

function refreshTags() {
  gPhotoTags = getTags();
  gPhotoTags = filterTags(gPhotoTags);
  gTagCounts = getTagCounts(gPhotoTags);
  displayTags(gTagCounts);
  updateImages(gPhotoTags);
  updateSearch();
}

// Create and insert the search bar
function initSearch() {
  var tabList = $('div[role="tablist"]');
  $(tabList).css("width", "95%");

  var searchBar = '<div id="tagSearch"><input id="searchInput"><p id="addSearch">+</p></div>'
  tabList.append(searchBar);

  $("#addSearch").click(function() {
    var search = $('#searchInput').val();
    if (search in gTagCounts) {
      toggleTag(search);
    }
    $('#searchInput').val("");
  });
}

// If new photos have been loaded refresh the tags
function checkImageCount() {
  var photoElements = $('li[class*="fbPhotoStarGridElement"]');
  var newPhotoCount = photoElements.length;
  if (gPhotoCount !== newPhotoCount) {
    gPhotoCount = newPhotoCount;
    refreshTags();
  }
}

// Extract tags from the images aria-labels
function getTags() {
  var ariaLabelLinks = $('a[aria-label]');
  var linkFullTag = {};
  for (var i = 0; i < ariaLabelLinks.length; i += 1) {
    var link = ariaLabelLinks[i];
    var linkLabel = link.getAttribute("aria-label");
    if (linkLabel.includes("Image may contain:")) {
      var photoId = link.getAttribute("id");
      // Remove "pic_" part of the id
      photoId = photoId.substring(4);
      // Remove "Image may contain: "
      linkFullTag[photoId] = linkLabel.substring(19);
    }
  }

  return parseTags(linkFullTag);
}

// Loop through each photos tag string and split into an array
function parseTags(linkFullTag) {
  $.each(linkFullTag, function(id) {
    var photoTagString = linkFullTag[id];
    linkFullTag[id] = parseTag(photoTagString);
  });

  return linkFullTag;
}

/*
Example Tag Strings:
"people sitting, table and food"
"1 person, sitting"
"people sitting"
"one or more people, sky, tree, outdoor and nature"
Converts to ["people", "sky", "tree", "outdoor", "nature"]
*/
function parseTag(fullTag) {
  var parsedTags = [];
  fullTag = fullTag.replace(" and ", ", ");
  var splitTag = fullTag.split(",");

  for (var i = 0; i < splitTag.length; i += 1) {
    var tagChunk = splitTag[i].trim();
    if (tagChunk.includes("people")) {
      if (parsedTags.indexOf("people") === -1) {
        parsedTags.push("people");
      }
    } else if (tagChunk.includes("person")) {
      if (parsedTags.indexOf("person") === -1) {
        parsedTags.push("person");
      }
    } else if (parsedTags.indexOf(tagChunk) === -1) {
      parsedTags.push(tagChunk);
    }
  }

  return parsedTags;
}

// Count how many times each tag occurs
function getTagCounts(photoTags) {
  var counts = {};

  $.each(photoTags, function(id) {
    var tagList = photoTags[id];
    for (var i = 0; i < tagList.length; i += 1) {
      var tag = tagList[i];
      if (!(tag in counts)) {
        counts[tag] = 0;
      }
      counts[tag] += 1;
    }
  });

  return counts;
}

// Generate tag bar
function displayTags(tagCounts) {
  // If tag bar already exists remove it
  var oldTagDiv = $('#tagDiv');
  if (oldTagDiv.length === 1) {
    oldTagDiv.remove();
  }

  var photoNavigation = $('div[data-referrer="timeline_collections_section_title"]')[0];
  $(photoNavigation).css("margin-bottom", 0);

  var sortedTagCounts = [];
  $.each(tagCounts, function(name) {
    var count = tagCounts[name];
    sortedTagCounts.push({name, count});
  });

  // Sort tags by frequency so more common tags show up first
  sortedTagCounts.sort(function(a, b) {
    if (a.count === b.count) {
      if (gSelectedTags.indexOf(a.name) !== -1 && gSelectedTags.indexOf(b.name) === -1) {
        return -1;
      } else if (gSelectedTags.indexOf(a.name) === -1 && gSelectedTags.indexOf(b.name) !== -1) {
        return 1;
      }
    }
    return b.count - a.count;
  });

  // Create actual <ul> list
  var listItems = "";
  for (var i = 0; i < sortedTagCounts.length; i += 1) {
    var tagObj = sortedTagCounts[i];
    var style = '';
    if (gSelectedTags.indexOf(tagObj.name) !== -1) {
      style = 'style="background-color: #4aa0f7"';
    }
    // String templates because I didn't want to learn the correct way ¯\_(ツ)_/¯
    var listItemText = `<p id="tagName">${tagObj.name}</p><p id="tagCount">${tagObj.count}</p>`;
    listItems += `<li class="tagListItem" ${style} label="${tagObj.name}">${listItemText}</li>`;
  }
  var listElement = `<ul id="tagList">${listItems}</ul>`;
  var tagDiv = `<div id="tagDiv">${listElement}</div>`;

  $(photoNavigation).after(tagDiv);

  // Set click listener on each tag
  var tagItems = $('.tagListItem');
  for (var i = 0; i < tagItems.length; i += 1) {
    var tagItem = tagItems[i];
    var tagLabel = tagItem.getAttribute("label");
    $(tagItem).click({tagLabel}, function(event) {
      toggleTag(event.data.tagLabel);
    });
  }
}

// Add the tag to the selected tags and refresh
function toggleTag(tagName) {
  var tagIndex = gSelectedTags.indexOf(tagName);
  if (tagIndex === -1) {
    gSelectedTags.push(tagName);
  } else {
    gSelectedTags.splice(tagIndex, 1);
  }

  refreshTags();
}


// Only keep photos who contain every tag currently selected
function filterTags(photoTags) {
  var filteredTags = {};

  $.each(photoTags, function(id) {
    var tagList = photoTags[id];
    var hasSelectedTags = true;
    for (var i = 0; i < gSelectedTags.length; i += 1) {
      var selectedTag = gSelectedTags[i];
      if (tagList.indexOf(selectedTag) === -1) {
        hasSelectedTags = false;
      }
    }

    if (hasSelectedTags) {
      filteredTags[id] = tagList;
    }
  });

  return filteredTags;
}

// Only display images that passed the filtering step
function updateImages(photoTags) {
  var photoElements = $('li[class*="fbPhotoStarGridElement"]');
  gPhotoCount = photoElements.length;

  var visibleElements = 0;
  for (var i = 0; i < photoElements.length; i += 1) {
    var photoElem = photoElements[i];
    var photoId = photoElem.getAttribute('data-fbid');
    $(photoElem).css("position", "relative");
    $(photoElem).css("margin-left", "0");
    $(photoElem).css("margin-top", "0");
    if (photoId in photoTags) {
      $(photoElem).css("display", "inline-block");
      visibleElements += 1;
    } else {
      $(photoElem).css("display", "none");
    }
  }

  updateGridHeight(visibleElements);
}

// Update the height of the image container to match the visible photos
function updateGridHeight(photoCount) {
  var photoGrid = $('ul[class*="fbStarGrid"]')[0];

  var newHeight = Math.ceil(photoCount / 4) * 211;
  $(photoGrid).css("height", `${newHeight}px`);
  $(photoGrid).css("max-height", `${newHeight}px`);
}

// Update the search recommendations with the new current tags
function updateSearch() {
  $("#searchInput").autocomplete({
    source: Object.keys(gTagCounts),
    select: function(event, ui) {
      var search = ui.item.label;
      if (search in gTagCounts) {
        toggleTag(search);
      }
      event.preventDefault();
      $("#searchInput").val("");
    }
  });
}
